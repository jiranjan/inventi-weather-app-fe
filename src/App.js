import React, { Component } from 'react';
import styled from 'styled-components';
import axios from 'axios';

import WeatherCard from './components/WeatherCard';
import ForecastCard from './components/ForecastCard';

const Wrapper = styled.div`
  display: flex;
  flex: 1;
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 70%;
  height: 90%;
  margin: auto;
`;

const InputContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: center;
  margin-top: 20px;
`;

const SearchInput = styled.input`
  background: rgb(120, 179, 145);
  height: 40px;
  width: 100%;
  align-self: center;
  border: none;
  border-radius: 20px;
  outline: none;
  padding-left: 20px;
  font-size: 18px;
  font-weight: 700;
  color: #FFF;
  display: flex;
`;

const SearchButton = styled.button`
  align-items: center;
  justify-content: center;
  background: rgb(120, 179, 145);
  color: #FFF;
  font-size: 16px;
  outline: none;
  font-weight: 700;
  border: none;
  display: flex;
  width: 100px;
  height: 40px;
  margin-left: 20px;
  border-radius: 20px;
`;

const Heading = styled.h2`
  color: #FFF;
`;

const WeatherCardsContainer = styled.div`
  display: flex;
`;

const ForecastCardsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

class App extends Component {
  constructor(props) {
    super(props);
    this.state={
      city: 'Prague',
      weatherCurrent: null,
      cityInfo: null,
      forecast: [],
      isError: false
    };
  };

  componentDidCatch(error) {
    console.error(error);
    this.setState({ isError: true });
  };

  async componentDidMount() {
    await this.getWeatherByCity();
    await this.getForecastByCity();
  };

  updateState = (e) => this.setState({[e.target.name]: e.target.value});

  getWeatherByCity = () => {
    const { city } = this.state;
    axios.get(`${process.env.REACT_APP_API_ENDPOINT}/weather/${city.split(' ').join('+')}`)
      .then(async response => {
        const { data } = await response;
        this.setState({ weatherCurrent: data[0], cityInfo: data[1]})
      })
      .catch(err =>  window.alert(err.message))
  };

  getForecastByCity = () => {
    const { city } = this.state;
    axios.get(`${process.env.REACT_APP_API_ENDPOINT}/weather/forecast/${city.split(' ').join('+')}&cnt=4`)
      .then(async response => {
        const { data } = await response;
        this.setState({ forecast: data.list })
      })
      .catch(err =>  window.alert(err.message))
  };

  getData = async () => {
    await this.getWeatherByCity();
    await this.getForecastByCity();
  };

  render() {
    const { city, weatherCurrent, cityInfo, isError, forecast } = this.state;
    return (
      <Wrapper>
        <Container>
          <InputContainer>
            <SearchInput
              value={city}
              name="city"
              onChange={this.updateState} />
            <SearchButton onClick={this.getData}>Search</SearchButton>
          </InputContainer>
          {
            isError ?
            <Heading>AJAJ. Spadlo to</Heading>
            :
            <div>
              <Heading>Today</Heading>
              <WeatherCardsContainer>
                <WeatherCard weatherCurrent={weatherCurrent} cityInfo={cityInfo} />
              </WeatherCardsContainer>
              <br />
              <Heading>Forecast</Heading>
              <ForecastCardsContainer>
                {
                  forecast.map((item, index) => <ForecastCard key={index} data={item} />)
                }
              </ForecastCardsContainer>
            </div>
        }
        </Container>
      </Wrapper>
    );
  };
};

export default App;

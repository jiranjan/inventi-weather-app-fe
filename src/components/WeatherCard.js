import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex: 1;
  height: 300px;
  justify-content: space-between;
`;

const WeatherContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const InfoContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const ImageContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const WeatherImage = styled.img`
  width: 100px;
`;

const Temp = styled.h2`
  color: #FFF;
`;

const TextContainer = styled.div`
  display: flex;
  flex-direction: row;
  height: 20px;
`;

const Additional = styled.h5`
  color: #FFF;
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
`;

const Key = styled.h5`
  font-size: 16;
  color: #FFF;
  margin-top: 0px;
  margin-bottom: 10px;
`;

const Value = styled.h5`
  font-size: 16;
  font-weight: 400;
  color: #FFF;
  margin-top: 0px;
  margin-bottom: 10px;
`;

const Flag = styled.img`
  width: 150px;
  height: 75px;
  margin-bottom: 20px;
`;

const WeatherCard = ({ weatherCurrent, cityInfo }) => {
  if (
    weatherCurrent === null
    || weatherCurrent === undefined
  ) return <div />
  const { weather, main } = weatherCurrent;
  console.log(cityInfo);
  return (
    <Wrapper>
      <WeatherContainer>
        <ImageContainer>
          <WeatherImage src={`http://openweathermap.org/img/wn/${weather[0].icon}@2x.png`} />
          <Temp>{main.temp}°C</Temp>
        </ImageContainer>
        {
          Object.entries(main).map(([key, value], index) => (
            <TextContainer key={index}>
              <Additional>{key}: </Additional>
              <Additional> {value}</Additional>
            </TextContainer>
          ))
        }
      </WeatherContainer>
      <InfoContainer>
        <Flag src={cityInfo[0].flag} />
        <Row>
          <Key>Alpha Code: </Key>
          <Value>{cityInfo[0].alpha2Code}</Value>
        </Row>
        <Row>
          <Key>Timezone: </Key>
          <Value>{cityInfo[0].timezones[0]}</Value>
        </Row>
        <Row>
          <Key>Country: </Key>
          <Value>{cityInfo[0].nativeName}</Value>
        </Row>
        {
          cityInfo[0].currencies.map((item, index) => (
            <Row key={index}>
              <Key>Currency: </Key>
              <Value>{item.name}</Value>
            </Row>
          ))
        }
        {
          cityInfo[0].languages.map((item, index) => (
            <Row key={index}>
              <Key>{index > 0 ? 'Second language: ' : 'Language'}</Key>
              <Value>{item.name}</Value>
            </Row>
          ))
        }
      </InfoContainer>
    </Wrapper>
  );
};

export default WeatherCard;

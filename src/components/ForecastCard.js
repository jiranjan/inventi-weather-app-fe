import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  flex: 4;
  background: #78B391;
  background: linear-gradient(to bottom, #78B391, #2f5e43);
  border-radius: 10px;
  height: 300px;
  margin: 0 10px;
  justify-content: center;
`;

const WeatherContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const InfoContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const ImageContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const WeatherImage = styled.img`

`
const Temp = styled.h3`
  color: #FFF;
`;

const AdditionlData = styled.div`
  display: flex;
  justify-content: space-between;
  color: #FFF;
  margin: 0;
`

const Additional = styled.h5`
  color: #FFF;
`;


const ForecastCard = ({ data }) => {
  return (
    <Wrapper>
      <WeatherContainer>
        <ImageContainer>
          <WeatherImage src={`http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`} />
        </ImageContainer>
          <Temp>{data.main.temp}°C</Temp>
          <AdditionlData><b>Pressure:</b>{data.main.pressure}</AdditionlData>
          <AdditionlData><b>Humidity:</b>{data.main.humidity}</AdditionlData>
          <AdditionlData><b>Temp min.:</b>{data.main.temp_min}°C</AdditionlData>
          <AdditionlData><b>Temp max.:</b>{data.main.temp_max}°C</AdditionlData>
      </WeatherContainer>
    </Wrapper>
  );
};

export default ForecastCard;
